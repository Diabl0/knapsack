# Knapsack Example

Simple example of [Knapsack problem](https://en.wikipedia.org/wiki/Knapsack_problem) implementation with basic Laravel UI

## Instalation

```
git clone git@bitbucket.org:Diabl0/knapsack.git
cd knapsack
composer install
touch database/database.sqlite
php artisan migrate:refresh --seed
```