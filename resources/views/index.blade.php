@extends('layouts.app')

@section('content')


    @include('common.errors')

    <div class="panel-body">

        <div class="panel panel-default">
            <div class="panel-heading">Calculate basket</div>
            <div class="panel-body">
                <form action="{{ url('basket') }}" method="GET" class="form-horizontal">
                    <div class="form-group">
                        <label for="amount" class="col-sm-3 control-label">Amount</label>
                        <input type="text" name="amount" class="form-control" value="98">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">
                            <i class="fa"></i> Calculate basket
                        </button>
                    </div>
                </form>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">Add price</div>
            <div class="panel-body">
                <form action="{{ url('price') }}" method="POST" class="form-horizontal">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <label for="price_pln" class="col-sm-3 control-label">Price</label>
                        <input type="text" name="price_pln" id="price_pln-name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="scalar" class="col-sm-3 control-label">Scalar</label>
                        <input type="text" name="scalar" id="scalar-name" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-plus"></i> Add Price
                        </button>
                    </div>
                </form>
            </div>
        </div>

        @if (count($prices) > 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Current Prices
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">

                        <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Price</th>
                            <th>Price per unit</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($prices as $price)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <div>{{ $price->scalar }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $price->price_pln }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $price->price_pln / $price->scalar }}</div>
                                </td>

                                <td>
                                    <a href="{{ url('price/'.$price->price_id) }}" class="btn btn-primary"><i
                                                class="fa fa-trash"></i> Edit</a>
                                    <form action="{{ url('price/'.$price->price_id) }}" method="POST"
                                          style="display: inline;">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}

                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

    </div>

@endsection