@extends('layouts.app')

@section('content')

        <!-- Bootstrap Boilerplate... -->

<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')

    <div class="panel panel-default">
        <div class="panel-heading">Edit price</div>
        <div class="panel-body">
            <form action="{{ url('price', $price->price_id ) }}" method="POST" class="form-horizontal">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="price_pln" class="col-sm-3 control-label">Price</label>
                    <input type="text" name="price_pln" id="price_pln-name" class="form-control"
                           value="{{ $price->price_pln }}">
                </div>
                <div class="form-group">
                    <label for="scalar" class="col-sm-3 control-label">Scalar</label>
                    <input type="text" name="scalar" id="scalar-name" class="form-control" value="{{ $price->scalar }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
