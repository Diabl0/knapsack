@extends('layouts.app')

@section('content')

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')

        <div>Best quantity: <b>{{ $result['quantity'] }}</b></div>
        <div>Best price: <b>{{ $result['price_pln'] }}</b></div>


        <div class="panel panel-default">
            <div class="panel-heading">
                Result
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover">

                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>Scalar</th>
                        <th>Price</th>
                        <th>Count</th>
                        <th>Value</th>
                    </tr>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                    @foreach ($result['itemsAgregated'] as $item)
                        <tr>
                            <!-- Task Name -->
                            <td class="table-text">
                                <div>{{ $item['scalar'] }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $item['price_pln'] }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $item['count'] }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $item['count'] * $item['price_pln'] }}</div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection