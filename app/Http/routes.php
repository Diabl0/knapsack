<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        $prices = \App\Price::orderBy('scalar', 'asc')->get();

        return view('index', [
            'prices' => $prices
        ]);
    });

    Route::post('/price', function (Request $request) {

        $validator = Validator::make($request->all(), [
            'price_pln' => 'required|numeric|max:255',
            'scalar'    => 'required|numeric|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $price             = new \App\Price();
        $price->product_id = 1;
        $price->price_pln  = $request->price_pln;
        $price->scalar     = $request->scalar;
        $price->save();

        return redirect('/')->with('status', ['success', 'Price added']);
    });

    Route::get('/price/{price}', function (Request $request, \App\Price $price) {
        return view('edit', [
            'price' => $price,
        ]);
    });

    Route::post('/price/{price}', function (Request $request, \App\Price $price) {
        $validator = Validator::make($request->all(), [
            'price_pln' => 'required|numeric|max:255',
            'scalar'    => 'required|numeric|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $price->price_pln = $request->price_pln;
        $price->scalar    = $request->scalar;
        $price->save();

        return redirect('/')->with('status', ['success', 'Price edited']);
    });

    Route::delete('/price/{price}', function (\App\Price $price) {
        $price->delete();
        return redirect('/')->with('status', ['warning', 'Price deleted']);
    });

    Route::get('/basket', function (Request $request) {
        $amount = $request->amount;

        $prices = \App\Price::where('product_id', 1)->get();
        /* @var $prices Illuminate\Database\Eloquent\Collection */

        $knapsack = new \App\Knapsack($prices->toArray(), $amount);

        $result = $knapsack->getResultAsArray();

        return view('result', [
            'result' => $result,
        ]);

    });

});
