<?php

namespace App;

class Knapsack
{
    /**
     * tablica z lista wynikow
     *
     * @var array
     */
    private $_result;

    /**
     * ile nas wszystko bedzie kosztowalo
     *
     * @var int
     */
    private $_price;

    /**
     * ile wszystkiego kupimy
     *
     * @var int
     */
    private $_quantity;

    /**
     * Tablica z cenami
     *
     * muszą być w niej conajmniej dwa klucze $this->_scalar_field i $this->_price_field
     *
     * @var array
     */
    private $_prices;

    /**
     * Nazwa klucza w tablicy $_prices zawierającego cenę
     *
     * @var string
     */
    private $_price_field = 'price_pln';

    /**
     * Nazwa klucza w tablicy $_prices zawierającego skalar (ilość sztuk w opakowaniu)
     *
     * @var string
     */
    private $_scalar_field = 'scalar';

    /**
     * @var string
     */
    private $_itemCount_field = '_count_';

    /**
     * Konstruktor parametryczny
     *
     * @param array $prices   tablica cen, musza byc w niej conajmniej dwa klucze $this->_scalar_field i $this->_price_field
     * @param int   $quantity ilosc jaka potrzebujemy (ilosc szt, litrow, jakiejs innej jednostki)
     * @param array $options  dodatkowe opcje (konfiguracja)
     */
    public function __construct($prices, $quantity, $options = [])
    {
        if (!empty($options)) {
            $this->_price_field     = isset($options['price_field']) ? $options['price_field'] : $this->_price_field;
            $this->_scalar_field    = isset($options['scalar_field']) ? $options['scalar_field'] : $this->_scalar_field;
            $this->_itemCount_field = isset($options['itemCount_field']) ? $options['itemCount_field'] : $this->_itemCount_field;
        }

        $this->_prices = $prices;

        $this->_result = $this->_algorytm($prices, $quantity);
        $this->_compute();
    }

    /**
     * Funkcja rekurencyjna realizujaca caly algorytm
     *
     * KROKI ALGORYTMU:
     * 1. Sortujemy tablice z cenami i skalarami produktow rosnaco poprzez cene jednostkowa
     * 2. Bierzemy taki skalar, ktory jest nie wiekszy niz ilosc i dodajemy do listy rozwiazan. Od ilosci odejmujemy skalar.
     *    Krok 2 powtarzamy dopoki mozemy.
     * 3. Jezeli ilosc > 0 to dopelniamy najmniejszym mozliwym opakowaniem.
     * 4. Z ustalonego rozwiazania liczymy cene jednostkowa.
     * 5. Z listy cen ($array) usuwamy te pozycje, ktorych cena jednostkowa > niz cena jednostkowa naszego rozwiazania
     * 6. Sprawdzamy czy otrzymane rozwiazanie jest naszym optimum (przez porownanie). Jezeli jest to pierwsze rozwiazanie to staje
     *    sie ono naszym optimum.
     * 7. Wracamy do punktu 2, chyba ze nie usunelismy zadnego elementu z tablicy cen ($array), badz tablica cen jest pusta.
     * 8. Zwracamy rozwiazanie oznaczone jako optimum.
     *
     * @param $prices   - array - lista cen, musi zawierac klucze [$this->_price_field] i [$this->_scalar_field]
     * @param $quantity - int - ilosc danego produktu (ile chcemy szt, litrow etc.)
     * @param $previous -  array - tablica wynikow z poprzedniej iteracji, w celu porownania
     * @return array - tablica z propozycja wynikow
     * @internal param $factor - float - wspolczynnik (cena jednostkowa) z poprzedniej iteracji
     */
    private function _algorytm($prices, $quantity, $previous = null)
    {

        $returner = [];

        //---------------------------------------
        //KROK 1
        //---------------------------------------
        $pricesByScalar = $prices; //tablica posortowana wedlug skalarow
        usort($prices, [$this, '_compareFactor']);
        usort($pricesByScalar, [$this, '_compare']);
        //---------------------------------------

        //---------------------------------------
        //KROK 2
        //---------------------------------------
        $uantityLeft = $quantity;
        while ($uantityLeft > 0) //dopoki mozemy dodawac rzeczy do tablicy wynikow bo nie przekraczamy ilosci
        {
            foreach ($prices as $box) {
                if ($uantityLeft - $box[$this->_scalar_field] >= 0) {
                    $uantityLeft -= $box[$this->_scalar_field];
                    $returner[] = $box;
                    break;
                }
            }

            //---------------------------------------
            //KROK 3
            //---------------------------------------
            //jezeli przekroczymy ilosc dodajac ostatni element (o najmniejszym skalarze) a ilosc w tym momencie nie wynosi 0 to dodajemy
            if ($uantityLeft - $pricesByScalar[count($pricesByScalar) - 1][$this->_scalar_field] < 0 && $uantityLeft != 0) {
                $uantityLeft -= $pricesByScalar[count($pricesByScalar) - 1][$this->_scalar_field];
                $returner[] = $pricesByScalar[count($pricesByScalar) - 1];
            }
            //---------------------------------------
        }
        //---------------------------------------

        //---------------------------------------
        //KROK 4
        //---------------------------------------
        $factor = $this->_factor($returner);

        //---------------------------------------

        //---------------------------------------
        //KROK 5
        //---------------------------------------
        $i = 0;
        foreach ($prices as $key => $box) {
            if ($box[$this->_price_field] / $box[$this->_scalar_field] > $factor) {
                unset($prices[$key]);
                ++$i;
            }
        }
        //---------------------------------------


        //warunek skonczenia algorytmu: nic nie usunelismy z tablicy cen, lub tablica cen jest pusta
        if ($i == 0 || empty($prices)) {
            //---------------------------------------
            //KROK 6
            //---------------------------------------
            if ($this->_isBetter($returner, $previous)) {
                return $returner;
            } else {
                return $previous;
            }
            //---------------------------------------
        } else {
            //---------------------------------------
            //KROK 7
            //---------------------------------------
            $returner = $this->_algorytm($prices, $quantity, $returner);
            //---------------------------------------
        }

        //---------------------------------------
        //KROK 6
        //---------------------------------------
        if ($this->_isBetter($returner, $previous)) {


            return $returner;
        } else {

            return $previous;
        }
        //---------------------------------------
    }

    /**
     * Funkcja pomocnicza, obliczamy wspolczynnik (cene jednostkowa) z danej tablicy wyników
     *
     * @param $resultSet - array - tablica danych wejsciowych z kluczami [$this->_scalar_field] i [$this->_price_field]
     * @return float - wspolczynnik
     */
    private function _factor($resultSet)
    {
        $sum_quantity = 0;
        $sum_price    = 0;
        foreach ($resultSet as $box) {
            $sum_quantity += $box[$this->_scalar_field];
            $sum_price += $box[$this->_price_field];
        }

        return $factor = $sum_price / $sum_quantity;
    }


    /**
     * Funkcja porownujaca, ktora stwierdza ktora instancja rozwiazania jest lepsza (tansza)
     *
     * @param $a - array/null - tablica z elementami 1 rozwiazania
     * @param $b - array/null - tablica z elementami 2 rozwiazania
     * @return boolean - true jezeli a jest lepsze, false jezeli b jest lepsze
     */
    private function _isBetter($a, $b)
    {
        if ($a === null) {
            return false;
        }

        if ($b === null) {
            return true;
        }

        $sum_price_a    = 0;
        $sum_quantity_a = 0;
        foreach ($a as $item) {
            $sum_price_a += $item[$this->_price_field];
            $sum_quantity_a += $item[$this->_scalar_field];
        }

        $sum_price_b    = 0;
        $sum_quantity_b = 0;
        foreach ($b as $item) {
            $sum_price_b += $item[$this->_price_field];
            $sum_quantity_b += $item[$this->_scalar_field];
        }

        if ($sum_price_a < $sum_price_b) {
            return true;
        } else {
            if ($sum_price_a == $sum_price_b) {
                if ($sum_quantity_a > $sum_quantity_b) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Podlicza ostateczne wartości znalezionego rozwiązania
     */
    private function _compute()
    {
        $price    = 0;
        $quantity = 0;
        foreach ($this->_result as $result) {
            $price += $result[$this->_price_field];
            $quantity += $result[$this->_scalar_field];
        }

        $this->_price    = $price;
        $this->_quantity = $quantity;
    }

    /**
     * Zwraca sama tablice z lista wynikow
     *
     * @return array - lista wynikow
     */
    public function getResultAsItemsList()
    {
        return $this->_result;
    }

    /**
     * Zwraca tablice z lista wynikow razem z sumaryczna iloscia i cena
     *
     * @return array - lista wynikow, sumaryczna cena i ilosc
     */
    public function getResultAsArray()
    {
        $return                      = [];
        $return[$this->_price_field] = $this->_price;
        $return['quantity']          = $this->_quantity;
        $return['items']             = $this->_result;

        $items      = [];
        $itemsCount = [];
        foreach ($this->_result as $item) {
            $items[$item['scalar']]          = $item;
            $itemsCount[$item['scalar']]     = (!isset($itemsCount[$item['scalar']]) ? 0 : $itemsCount[$item['scalar']]) + 1;
            $items[$item['scalar']]['count'] = $itemsCount[$item['scalar']];

        }
        $return['itemsAgregated'] = $items;

        return $return;
    }

    /**
     * callback porownujacy skalary
     *
     * @param $a - array - element 1 do porownania (tablica z kluczem [$this->_scalar_field])
     * @param $b - array - element 2 do porownania (tablica z kluczem [$this->_scalar_field])
     * @return int 1/-1
     */
    private function _compare($a, $b)
    {
        if ($a[$this->_scalar_field] == $b[$this->_scalar_field]) {
            return 0;
        }
        return ($a[$this->_scalar_field] < $b[$this->_scalar_field]) ? 1 : -1;
    }

    /**
     * callback porownujacy ceny jednostkowe (wspolczynniki)
     *
     * @param $a - array - element 1 do porownania (tablica z kluczem [$this->_scalar_field] i kluczem [$this->_price_field])
     * @param $b - array - element 2 do porownania (tablica z kluczem [$this->_scalar_field] i kluczem [$this->_price_field])
     * @return int 1/-1
     */
    private function _compareFactor($a, $b)
    {
        if ($a[$this->_price_field] / $a[$this->_scalar_field] == $b[$this->_price_field] / $b[$this->_scalar_field]) {
            return 0;
        }
        return ($a[$this->_price_field] / $a[$this->_scalar_field] > $b[$this->_price_field] / $b[$this->_scalar_field]) ? 1 : -1;

    }
}