<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert(
            [
                'product_id' => 1,
                'scalar'     => 1,
                'price_pln'  => 10,
            ]);
        DB::table('prices')->insert(
            [
                'product_id' => 1,
                'scalar'     => 5,
                'price_pln'  => 49,
            ]);
        DB::table('prices')->insert(
            [
                'product_id' => 1,
                'scalar'     => 10,
                'price_pln'  => 95,
            ]);
        DB::table('prices')->insert(
            [
                'product_id' => 1,
                'scalar'     => 25,
                'price_pln'  => 240,
            ]);
        DB::table('prices')->insert(
            [
                'product_id' => 1,
                'scalar'     => 50,
                'price_pln'  => 450,
            ]);


    }
}
